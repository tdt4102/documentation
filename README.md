# Docs

Dette er repoet som holder dokumentasjonen til faget. Det er satt opp gitlab ci, så alle endringer på main branch reflekteres til den offisielle dokumentasjonen på push eller godkjent pull request.

Nettsiden finnes på <https://tdt4102.pages.stud.idi.ntnu.no/documentation/>

## Hvordan kjøre lokalt

`pip install -r requirements.txt` - Installer de nødvendige bibliotekene
`mkdocs serve` - Start en lokal server med live-reloading
`mkdocs build` - Kompiler filene lokalt

Dersom python scripts ikke er på path kan du bruke `python -m mkdocs`, men jeg anbefaler på det sterkeste å legge de til på path. Pip gir warnings om dette på install.

# Markdown for devs

## Lists

```markdown
1. This is how to make a list
2. This text is **fat** og _cursive_
```

Result:

1. This is how to make a list
2. This text is **fat** og _cursive_

## Keystrokes

Keystrokes tell the reader how to enter hotkeys

```markdown
Press ++ctrl+shift+p++ to open the command pallette
```

Result:  
Press ++ctrl+shift+p++ to open the command pallette

## Code blocks

Code blocks work as follows. The number specifies the number of the first line. It should usually be 1. You can highlight one or more lines by using `hl lines`.

````markdown
```c++ linenums="23" hl_lines="2 4"
int main() {
AnimationWindow\* win = new AnimationWindow("Vindu", 1000, 800, 100, 200);
return 0; // Kommentar
}
```
````

Result:

```c++ linenums="23" hl_lines="2 4"
int main() {
AnimationWindow\* win = new AnimationWindow("Vindu", 1000, 800, 100, 200);
return 0; // Kommentar
}
```

### Inline code

```
The language of inline code can be specified like this: `#!c++ draw_rectangle(100, 100, 20, 20);`
```

Result:  
The language of inline code can be specified like this: `#!c++ draw_rectangle(100, 100, 20, 20);`

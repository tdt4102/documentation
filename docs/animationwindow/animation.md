# Animasjon

Nå skal vi se hvorfor `AnimationWindow` har fått sitt navn, ved å tegne enkle animasjoner. 

En animasjon er en rekke bilder som er vist kort etter hverandre, hvor ved å bevege ting vist på bildene litt om gangen gjør at de ser ut som de beveger seg:

<img src="../../images/rotoscope.gif" alt="Bilde som viser en kurve tegnet med draw_arc funksjonen og endret farge" style="max-width:600px; width:100%;"/>

Vi har fram til nå bare tegnet enkelte bilder som ikke beveger eller endrer seg, og fordi animasjonen krever at vi tegner flere bilder trenger vi å bruke et par andre funksjoner i stede for `wait_for_close()` funksjonen som vi har brukt så langt:

```c++
#include "AnimationWindow.h"

int main() {
	TDT4102::AnimationWindow window;

	while(!window.should_close()) {
		window.draw_circle({100, 100}, 50, TDT4012::Color::dark_green);
		window.next_frame();
	}
	
	return 0;
}
```

Det er to nye funksjoner som brukes i dette eksempelet:

* `bool AnimationWindow::should_close()`: Returnerer `false` fram til brukeren lukker vinduet, og deretter `true`. `while`-løkken sørger for at programmet fortsetter fram til den er avsluttet.
* `void AnimationWindow::next_frame()`: Funksjonen gjør flere operasjoner:
	1. Bildet du har tegnet blir vist på skjermen
	2. Blidet blir tømmet slik at du kan tegne neste bildet i animasjonen
	3. Venter litt slik at vinduet ikke tegner flere enn 60 bilder per sekund

<img src="../../images/animation_1.png" alt="Bilde som viser en kurve tegnet med draw_arc funksjonen og endret farge" style="max-width:600px; width:100%;"/>

Siden det nå er mulig å tegne flere bilder på rad kan vi prøve å lage en enkel animasjon, for eksempel en firkant som beveger mot høyre siden av skjermen. Vi gjør dette ved å øke x-koordinatet hver gang vi tegner et nytt bilde:

```c++
#include "AnimationWindow.h"

int main() {
	TDT4102::AnimationWindow window;

	int xPosition = 0;
	while(!window.should_close()) {
		xPosition = xPosition + 2;
		TDT4102::Point position {xPosition, 100};
		window.draw_rectangle(position, 100, 100);

		window.next_frame();
	}
	
	return 0;
}
```

Merk at hele bildet må tegnes på nytt etter hver gang `next_frame()` funksjonen blir kallet. Det er ikke mulig å beholde en del av bildet og tegne bare det som endrer seg. 

Når du kjører eksempelet forsvinner firkanten etterhvert. Vi kan løse dette ved å legge til en if setning:

```c++
#include "AnimationWindow.h"

int main() {
	TDT4102::AnimationWindow window;

	int xPosition = 0;
	while(!window.should_close()) {
		xPosition = xPosition + 2;
		if(xPosition > 200) {
			xPosition = 0;
		}
		TDT4102::Point position {xPosition, 100};
		window.draw_rectangle(position, 100, 100);

		window.next_frame();
	}
	
	return 0;
}
```

Og for moro skyld kan vi sørge for at den bare flytter seg når mellomrom på tastaturen holdes ned (du finner mer informasjon om dette på input siden):

```c++
#include "AnimationWindow.h"

int main() {
	TDT4102::AnimationWindow window;

	int xPosition = 0;
	while(!window.should_close()) {
		if(window.is_key_down(KeyboardKey::SPACE)) {
			xPosition = xPosition + 2;
		}
		if(xPosition > 200) {
			xPosition = 0;
		}
		TDT4102::Point position {xPosition, 100};
		window.draw_rectangle(position, 100, 100);

		window.next_frame();
	}
	
	return 0;
}
```
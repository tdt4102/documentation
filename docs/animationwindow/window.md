# AnimationWindow

AnimationWindow er grafikkbiblioteket vi bruker i faget, både i øvingene og på eksamen. Det brukes til å tegne enkle former som sirkler og linjer, og å lage enkle grafiske brukergrensesnitt (GUI).

## Opprett et nytt vindu

For å kunne åpne et AnimationWindow vindu er det nødvendig å inkludere header-filen `AnimationWindow.h` øverst i .cpp filen der du ønsker å bruke AnimationWindow:

```c++
#include "AnimationWindow.h"
```

Ved å inkludere denne såkalte «header» filen forteller du C++ hva et AnimationWindow er, og lar deg bruke det i programmet ditt. Vi kommer til å forklare nærmere hvordan header filer fungerer i forelesningene. 

Her er et eksempelprogram som åpner et vindu:

```c++
#include "AnimationWindow.h"

int main() {
	TDT4102::AnimationWindow window;
	window.wait_for_close();
	return 0;
}
```

<img src="../images/window_simple.png" alt="Bilde av et tom vindu" style="max-width:400px; width:100%;"/>

I dette programmet er det to linjer som er relevant:

```c++
	TDT4102::AnimationWindow window;
	window.wait_for_close();
```

Den første linjen oppretter vinduet ved å deklarere en variabel med datatype «TDT4102::AnimationWindow». For å forstå meningen med den andre linjen, prøv å fjerne den og kjøre programmet. Noen gang så skjer det ingenting, mens andre ganger vises et vindu i en kort periode. Dette skjer fordi programmet fortsetter å kjøre etter vi oppretter `window` variabelen, og siden den har nå kommet til slutten av `main()` funksjonen, avsluttes programmet som lukker vinduet automatisk. Vi må derfor sørge for at programmet venter fram til brukeren lukker vinduet, som vi kan gjøre ved å bruke `wait_for_close()` funksjonen.


## Lukke et vindu
Du kan lukke vinduet ved å kalle medlemsfunksjonen `close()`. Dette kan for eksempel brukes i en callback funksjon til en quit-knapp.

## Endre vinduets standardverdier

AnimationWindow har forskjellige parametere som lar deg endre hvor på skjermen vinduet dukker opp, størrelsen, og tittelen som vises øverst på vinduet.

### Plassering på skjermen

Plassering på skjermen oppgis som x og y koordinat i piksler, målt fra vinduets og skjermens øverst venstre hjørne.

```c++
#include "AnimationWindow.h"

int main() {
	int windowPositionX = 300;
	int windowPositionY = 100;
	AnimationWindow window(windowPositionX, windowPositionY);

	window.wait_for_close();
	return 0;
}
```

Vinduet i dette eksempelet åpnes på koordinatene (300, 100):

<img src="../images/window_position.png" alt="Bilde av et tom vindu plassert på skjermen" style="max-width:600px; width:100%;"/>

### Størrelse

Vinduets størrelse defineres som bredde og høyde:

```c++
#include "AnimationWindow.h"

int main() {
	int windowPositionX = 300;
	int windowPositionY = 100;
	int windowWidth = 500;
	int windowHeight = 100;
	AnimationWindow window(windowPositionX, windowPositionY, windowWidth, windowHeight);

	window.wait_for_close();
	return 0;
}
```

Merk at det er obligatorisk å definere hvor vinduet skal plasseres på skjermen når du ønsker å endre størrelsen. 

Dette er hvordan vinduet ser ut når du kjører eksempelet:

<img src="../images/window_size.png" alt="Bilde av et tom vindu med endret størrelse" style="max-width:600px; width:100%;"/>

### Tittel

Hvis du har både definert vinduets posisjon og størrelsen har du mulighet å endre tittelen som vises øverst i vinduet ved å sette inn en streng:

```c++
#include "AnimationWindow.h"

int main() {
	int windowPositionX = 300;
	int windowPositionY = 100;
	int windowWidth = 500;
	int windowHeight = 100;
	std::string windowTitle = "Alternate window title";
	AnimationWindow window(windowPositionX, windowPositionY, windowWidth, windowHeight, windowTitle);

	window.wait_for_close();
	return 0;
}
```

Når du kjører eksempelet ser du at teksten øverst i vinduet har endret:

<img src="../images/window_title.png" alt="Bilde av et tom vindu med endret tittel" style="max-width:600px; width:100%;"/>




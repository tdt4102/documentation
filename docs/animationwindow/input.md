# Input



## Mus

For å finne ut hvor i vinduet musen befinner seg brukes det `get_mouse_coordinates()` funksjonen:

```c++
TDT4102::Point TDT4102::AnimationWindow::get_mouse_coordinates();
```

Merk at `AnimationWindow::` betyr at funksjonen er et medlemsfunksjon av `TDT4102::AnimationWindow` klassen. Funksjonen returnerer en instans av `TDT4102::Point` som representerer koordinatene hvor musen peker til relativ til øverst venstre hjørnet av vinduet (akkurat det samme koordinatsystem som brukes til tegning). Her er et eksempel som visualiserer koordinatene musene peker på:

```c++
#include "AnimationWindow.h"

int main() {
	TDT4102::AnimationWindow window;

	while(!window.should_close()) {
		TDT4102::Point mouseCoordinates = window.get_mouse_coordinates();

		std::string coordinateText = std::to_string(mouseCoordinates.x) + ", " + std::to_string(mouseCoordinates.y);
		window.draw_text(mouseCoordinates, coordinateText);

		window.next_frame();
	}
	
	return 0;
}
```

Du kan sjekke om høyre eller venstre musetast er trykket ned med: 

```c++
bool TDT4102::AnimationWindow::is_left_mouse_button_down() const;
bool TDT4102::AnimationWindow::is_right_mouse_button_down() const;

```

Funksjonen returnerer `true` dersom musetasten er nede.

## Tastatur

For å benytte deg av tastaturet kan du bruke `is_key_down()` funksjonen:

```c++
bool TDT4102::AnimationWindow::is_key_down(KeyboardKey key) const;
```

`is_key_down()` funksjonen tar inn en tast på tastaturet som parameter, og returnerer `true` når tasten holdes ned, og ellers `false`. Tasten defineres som et verdi av `KeyboardKey`. For eksempel, her brukes R tasten å endre fargen på et rektangel:

```c++
#include "AnimationWindow.h"

int main() {
	TDT4102::AnimationWindow window;

	while(!window.should_close()) {
		bool rKeyIsPressed = window.is_key_down(KeyboardKey::R);

		if(rKeyIsPressed) {
			window.draw_rectangle({100, 100}, 100, 100, Color::red);
		} else {
			window.draw_rectangle({100, 100}, 100, 100, Color::dark_green);
		}

		window.next_frame();
	}
	
	return 0;
}
```

### Alle tastene som finnes i KeyboardKey

Det finnes mange forskjellige taster som kan brukes i programmet. Her er et oversikt over alle. Merk at ikke alle tastatur har hver av disse tilgjengelig (for eksempel, mange tastatur på bærbare maskiner har ingen numpad).

<table>
<tr><td style="font-weight: bold;">Tast</td><td style="font-weight: bold;">KeyboardKey verdi</td></tr>
<tr><td>Bokstavtaster</td><td><code>KeyboardKey::A</code> til <code>KeyboardKey::Z</code></td></tr>
<tr><td>Norske bokstavtaster</td><td><code>KeyboardKey::AA</code><br>
<code>KeyBoardKey::AE</code><br><code>KeyboardKey::OO</code></td></tr>
<tr><td>Talltaster</td><td><code>KeyboardKey::KEY_0</code> til <code>KeyboardKey::KEY_9</code></td></tr>
<tr><td>Numpad: talltaster</td><td><code>KeyboardKey::NUMPAD_0</code> til <code>KeyboardKey::NUMPAD_9</code></td></tr>
<tr><td>Numpad: spesialtaster</td><td><code>KeyboardKey::NUMPAD_COMMA</code><br>
<code>KeyboardKey::NUMPAD_SLASH</code><br>
<code>KeyboardKey::NUMPAD_ASTERISK</code><br>
<code>KeyboardKey::NUMPAD_MINUS</code><br>
<code>KeyboardKey::NUMPAD_PLUS</code><br>
<code>KeyboardKey::NUMPAD_ENTER</code><br>
<code>KeyboardKey::NUMPAD_PERIOD</code><br>
<code>KeyboardKey::NUMPAD_EQUALS</code></td></tr>
<tr><td>Piltaster</td><td><code>KeyboardKey::LEFT</code><br>
<code>KeyboardKey::UP</code><br>
<code>KeyboardKey::RIGHT</code><br>
<code>KeyboardKey::DOWN</code></td></tr>
<tr><td>Shift</td><td><code>KeyboardKey::LEFT_SHIFT</code><br>
<code>KeyboardKey::RIGHT_SHIFT</code></td></tr>
<tr><td>Ctrl</td><td><code>KeyboardKey::LEFT_CTRL</code><br>
<code>KeyboardKey::RIGHT_CTRL</code></td></tr>
<tr><td>Windows tast<br>Cmd (på Mac)</td><td><code>KeyboardKey::SUPER_LEFT</code><br>
<code>KeyboardKey::SUPER_RIGHT</code></td></tr>
<tr><td>Alt</td><td><code>KeyboardKey::LEFT_ALT</code><br>
<code>KeyboardKey::RIGHT_ALT</code></td></tr>
<tr><td>Page Up/Down</td><td><code>KeyboardKey::PAGE_UP</code><br>
<code>KeyboardKey::PAGE_DOWN</code></td></tr>
<tr><td>Funksjonstastene</td><td><code>KeyboardKey::F1</code> til <code>KeyboardKey::F24</code></td></tr>
<tr><td>&lt; &gt;</td><td><code>KeyboardKey::LESS_THAN</code><br>
<code>KeyboardKey::GREATER_THAN</code></td></tr>
<tr><td>{ }</td><td><code>KeyboardKey::LEFT_PAREN</code><br>
<code>KeyboardKey::RIGHT_PAREN</code></td></tr>
<tr><td>[ ]</td><td><code>KeyboardKey::LEFT_BRACKET</code><br>
<code>KeyboardKey::RIGHT_BRACKET</code></td></tr>
<tr><td>Backspace</td><td><code>KeyboardKey::BACKSPACE</code></td></tr>
<tr><td>Tab</td><td><code>KeyboardKey::TAB</code></td></tr>
<tr><td>Enter</td><td><code>KeyboardKey::ENTER</code></td></tr>
<tr><td>Escape</td><td><code>KeyboardKey::ESCAPE</code></td></tr>
<tr><td>Mellomrom</td><td><code>KeyboardKey::SPACE</code></td></tr>
<tr><td>Home</td><td><code>KeyboardKey::HOME</code></td></tr>
<tr><td>End</td><td><code>KeyboardKey::END</code></td></tr>
<tr><td>Printscreen</td><td><code>KeyboardKey::PRINT_SCREEN</code></td></tr>
<tr><td>Insert</td><td><code>KeyboardKey::INSERT</code></td></tr>
<tr><td>Menu</td><td><code>KeyboardKey::MENU</code></td></tr>
<tr><td>Delete</td><td><code>KeyboardKey::DELETE</code></td></tr>
<tr><td>_</td><td><code>KeyboardKey::UNDERSCORE</code></td></tr>
<tr><td>&</td><td><code>KeyboardKey::AMPERSAND</code></td></tr>
<tr><td>*</td><td><code>KeyboardKey::ASTERISK</code></td></tr>
<tr><td>@</td><td><code>KeyboardKey::AT</code></td></tr>
<tr><td>^</td><td><code>KeyboardKey::CARET</code></td></tr>
<tr><td>:</td><td><code>KeyboardKey::COLON</code></td></tr>
<tr><td>$</td><td><code>KeyboardKey::DOLLAR</code></td></tr>
<tr><td>!</td><td><code>KeyboardKey::EXCLAMATION_MARK</code></td></tr>
<tr><td>#</td><td><code>KeyboardKey::HASH</code></td></tr>
<tr><td>%</td><td><code>KeyboardKey::PERCENT</code></td></tr>
<tr><td>'</td><td><code>KeyboardKey::QUOTE</code></td></tr>
<tr><td>"</td><td><code>KeyboardKey::DOUBLE_QUOTE</code></td></tr>
<tr><td>+</td><td><code>KeyboardKey::PLUS</code></td></tr>
<tr><td>;</td><td><code>KeyboardKey::SEMICOLON</code></td></tr>
<tr><td>.</td><td><code>KeyboardKey::PERIOD</code></td></tr>
<tr><td>-</td><td><code>KeyboardKey::MINUS</code></td></tr>
<tr><td>=</td><td><code>KeyboardKey::EQUALS</code></td></tr>
<tr><td>\</td><td><code>KeyboardKey::BACKSLASH</code></td></tr>
<tr><td>`</td><td><code>KeyboardKey::GRAVE</code></td></tr>
<tr><td>,</td><td><code>KeyboardKey::COMMA</code></td></tr>
<tr><td>/</td><td><code>KeyboardKey::SLASH</code></td></tr>
<tr><td>?</td><td><code>KeyboardKey::QUESTION_MARK</code></td></tr>
<tr><td>Pause</td><td><code>KeyboardKey::PAUSE</code></td></tr>
<tr><td>Scroll lock</td><td><code>KeyboardKey::SCROLL_LOCK</code></td></tr>
<tr><td>Caps lock</td><td><code>KeyboardKey::CAPS_LOCK</code></td></tr>
<tr><td>Num lock</td><td><code>KeyboardKey::NUM_LOCK</code></td></tr>
<tr><td>Media tastene</td><td><code>KeyboardKey::AUDIO_MUTE</code><br>
<code>KeyboardKey::AUDIO_NEXT</code><br>
<code>KeyboardKey::AUDIO_PLAY</code><br>
<code>KeyboardKey::AUDIO_PREV</code><br>
<code>KeyboardKey::AUDIO_STOP</code><br>
<code>KeyboardKey::AUDIO_VOLUME_UP</code><br>
<code>KeyboardKey::AUDIO_VOLUME_DOWN</code></td></tr>
</table>
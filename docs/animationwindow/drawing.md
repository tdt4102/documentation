# Tegning

## Generelt om tegning

### Vinduets koordinatsystem

Alle former som kan tegnes i vinduet krever at du spesifiserer en posisjon. Koordinater er målet relativ til øverst venstre hjørnet i vinduet, hvor x-koordinater øker mot høyre og y-koordinater øker nedover. 

<img src="../../images/window_coordinates.png" alt="Bilde som viser x og y-aksen i vinduet" style="max-width:600px; width:100%;"/>


Du kan også bruke følgende program for å visualisere hvilket koordinat musepekeren er på:

```c++
#include "AnimationWindow.h"

int main() {
	TDT4102::AnimationWindow window;

	while(!window.should_close()) {
		TDT4102::Point mouseCoordinates = window.get_mouse_coordinates();
		string coordinateText = std::to_string(mouseCoordinates.x) + ", " + std::to_string(mouseCoordinates.y);
		window.draw_text(mouseCoordinates, coordinateText);

		window.next_frame();
	}
	
	return 0;
}
```

### Rekkefølge av tegneoperasjoner

Du kan tenke på vinduet som et slags maleri. Det betyr at når du tegner over noe du har tegnet tidligere blir det «malt over».

For eksempel, dette er resultatet når rektangelen tegnes først og sirklen sist:

<img src="../../images/draw_order_1.png" alt="Grønn sirkel over et blått rektangel" style="max-width:600px; width:100%;"/>

Og dette er resultatet dersom rektangelen tegnes sist:

<img src="../../images/draw_order_2.png" alt="Blått rektangel over en grønn sirkel" style="max-width:600px; width:100%;"/>

## Basisformer

Om du ønsker å teste de kodeeksemplene som er vist, kan du bruke følgende eksempelet hvor du erstatter linjen som er markert:

```c++
#include "AnimationWindow.h"

int main() {
	TDT4102::AnimationWindow window;

	// Erstatt denne linjen med eksemplet som du ønsker å prøve

	window.wait_for_close();
}
```

### Sirkler

For å tegne sirkler kan du bruke `draw_circle()` funksjonen. Denne krever et punkt hvor midtpunktet skal tegnes, og en radius som definerer hvor stor den skal være:

```c++
	TDT4102::Point circleOrigin {150, 150};
	int radius = 100; 
	window.draw_circle(circleOrigin, radius);
```

```c++
	window.draw_circle({150, 150}, 100);
```

<img src="../../images/circle_1.png" alt="Bilde som viser en blå sirkel" style="max-width:450px; width:100%;"/>

Det er mulig å endre på sirklens farge:

```c++
	TDT4102::Point circleOrigin {150, 150};
	int radius = 100;
	TDT4102::Color fillColor = TDT4102::Color::dark_orange;
	window.draw_circle(circleOrigin, radius, fillColor);
```

<img src="../../images/circle_2.png" alt="Bilde som viser en mørke-oransje sirkel" style="max-width:450px; width:100%;"/>

Og en kantlinje med en gitt farge: 

```c++
	TDT4102::Point circleOrigin {150, 150};
	int radius = 100;
	TDT4102::Color fillColor = TDT4102::Color::dark_orange;
	TDT4102::Color borderColor = TDT4102::Color::black;
	window.draw_circle(circleOrigin, radius, fillColor, borderColor);
```

<img src="../../images/circle_3.png" alt="Bilde som viser en mørke-oransje sirkel med svart kantlinje" style="max-width:450px; width:100%;"/>

### Trekanter

Trekanter tegnes mellom tre punkt og `draw_triangle()` funksjonen:

```c++
	TDT4102::Point vertex0 {150, 50};
	TDT4102::Point vertex1 {50, 200};
	TDT4102::Point vertex2 {250, 200};
	window.draw_triangle(vertex0, vertex1, vertex2);
```

<img src="../../images/triangle_1.png" alt="Bilde som viser en gul trekant" style="max-width:450px; width:100%;"/>

Og som før kan fargen endres:

```c++
	TDT4102::Point vertex0 {150, 50};
	TDT4102::Point vertex1 {50, 200};
	TDT4102::Point vertex2 {250, 200};
	TDT4102::Color fillColor = TDT4102::Color::indigo;
	window.draw_triangle(vertex0, vertex1, vertex2, fillColor);
```

<img src="../../images/triangle_2.png" alt="Bilde som viser en lilla trekant" style="max-width:450px; width:100%;"/>

### Rektangel

Rektangel tegnes fra et punkt som tilsvarer øverst venstre hjørnet samt høyde og bredde. Den tegnes med `draw_rectangle()` funksjonen:

```c++
	TDT4102::Point topLeftCorner {50, 50};
	int width = 150;
	int height = 100;
	window.draw_rectangle(topLeftCorner, width, height);
```

<img src="../../images/rectangle_1.png" alt="Bilde som viser et blågrønt rektangel" style="max-width:450px; width:100%;"/>

Som alle andre former kan fargen endres:

```c++
	TDT4102::Point topLeftCorner {50, 50};
	int width = 150;
	int height = 100;
	TDT4102::Color fillColor = TDT4102::Color::light_sea_green;
	window.draw_rectangle(topLeftCorner, width, height, fillColor);
```

Og kantfargen kan også endres som i sirkler:

```c++
	TDT4102::Point topLeftCorner {50, 50};
	int width = 150;
	int height = 100;
	TDT4102::Color fillColor = TDT4102::Color::light_sea_green;
	TDT4102::Color edgeColor = TDT4102::Color::lime;
	window.draw_rectangle(topLeftCorner, width, height, fillColor, lime);
```
<img src="../../images/rectangle_2.png" alt="Bilde som viser et grønt rektangel" style="max-width:450px; width:100%;"/>

### Quad

En quad er et rektangel hvor hvert hjørne kan flyttes uavhengig av hverandre. Den tegnes med `draw_quad()` funksjonen:

```c++
	TDT4102::Point vertex0 {180, 80};
	TDT4102::Point vertex1 {50, 120};
	TDT4102::Point vertex2 {120, 250};
	TDT4102::Point vertex3 {250, 180};
	window.draw_quad(vertex0, vertex1, vertex2, vertex3);
```

<img src="../../images/quad_1.png" alt="Bilde som viser en lyseblå quad" style="max-width:450px; width:100%;"/>

Fargen kan justeres:

```c++
	TDT4102::Point vertex0 {180, 80};
	TDT4102::Point vertex1 {50, 120};
	TDT4102::Point vertex2 {120, 250};
	TDT4102::Point vertex3 {250, 180};
	TDT4102::Color fillColor = TDT4102::Color::violet;
	window.draw_quad(vertex0, vertex1, vertex2, vertex3, fillColor);
```

<img src="../../images/quad_2.png" alt="Bilde som viser en rosa quad" style="max-width:450px; width:100%;"/>

### Linjer

Du kan tegne linjer ved å bruke `draw_line()` funksjonen. Linjer tegnes mellom to punkt:

```c++
	TDT4102::Point lineStart {50, 50};
	TDT4102::Point lineEnd {280, 290};
	window.draw_line(lineStart, lineEnd);
```

<img src="../../images/line_1.png" alt="Bilde som viser en linje" style="max-width:450px; width:100%;"/>

I tillegg kan linjens farge endres:

```c++
	TDT4102::Point lineStart {50, 50};
	TDT4102::Point lineEnd {280, 290};
	TDT4102::Color lineColor = TDT4102::Color::firebrick;
	window.draw_line(lineStart, lineEnd, lineColor);
```

<img src="../../images/line_2.png" alt="Bilde som viser en linje" style="max-width:450px; width:100%;"/>

### Arc

En bue er en sirkel som tegnes delvis, mellom et start og sluttvinkel relativ til sirklens midtpunkt:

<img src="../../images/arc_angles.png" alt="Bilde som viser vinkler" style="max-width:600px; width:100%;"/>

Merk at sluttvinkelen må være større enn startvinkelen, og alle vinklene må være mellom 0 og 360 grader.

Høyden og bredden spesifiserer sirklens størrelse, og gjør det mulig å strekke sirklen:

<img src="../../images/arc_scale.png" alt="Bilde som viser ellipser" style="max-width:400px; width:100%;"/>

Med disse parameterene til sammen kan du tegne en bue ved å bruke `draw_arc()` funksjonen:

```c++
	TDT4102::Point centerPoint {100, 50};
	int width = 200;
	int height = 370;
    int startDegree = 270;
    int endDegree = 360;
	window.draw_arc(centerPoint, width, height, startDegree, endDegree);
```

<img src="../../images/arc_1.png" alt="Bilde som viser en kurve tegnet med draw_arc funksjonen" style="max-width:600px; width:100%;"/>

Lik som linjer er det mulig å endre fargen:

```c++
	TDT4102::Point centerPoint {100, 50};
	int width = 200;
	int height = 370;
    int startDegree = 270;
    int endDegree = 360;
	TDT4102::Color lineColor = TDT4102::Color::lime;
	window.draw_arc(centerPoint, width, height, startDegree, endDegree, lineColor);
```

<img src="../../images/arc_2.png" alt="Bilde som viser en kurve tegnet med draw_arc funksjonen og endret farge" style="max-width:600px; width:100%;"/>

## Avanserte former

### Tekst

For å tegne tekst i vinduet brukes `draw_text()` funksjonen. Denne tar inn et sted på skjermen, og en tekst streng som skal vises som parametere:

```c++
	TDT4102::Point location {100, 100};
	string message = "Greetings from outer space!";
	window.draw_text(location, message);
```

<img src="../../images/text_1.png" alt="Bilde som viser tekst" style="max-width:450px; width:100%;"/>

Du kan endre fargen på teksten:

```c++
	TDT4102::Point location {100, 100};
	string message = "Greetings from outer space!";
	TDT4102::Color textColor = TDT4102::Color::deep_skyblue;
	window.draw_text(location, message, textColor);
```

<img src="../../images/text_2.png" alt="Bilde som viser blå tekst" style="max-width:450px; width:100%;"/>

Og størrelsen:

```c++
	TDT4102::Point location {100, 100};
	string message = "Greetings from outer space!";
	TDT4102::Color textColor = TDT4102::Color::deep_skyblue;
	int fontSize = 120;
	window.draw_text(location, message, textColor, fontSize);
```

<img src="../../images/text_3.png" alt="Bilde som viser stor blå tekst" style="max-width:450px; width:100%;"/>

Om du ønsker skrifttypen kan du bruke en Font verdi som siste parameter: 

```c++
	TDT4102::Point location {100, 100};
	TDT4102::string message = "Greetings from outer space!";
	Color textColor = TDT4102::Color::deep_skyblue;
	int fontSize = 120;
	TDT4102::Font fontFace = TDT4102::Font::times_bold;
	window.draw_text(location, message, textColor, fontSize, fontFace);
```

<img src="../../images/text_4.png" alt="Bilde som viser stor blå tekst med endret font" style="max-width:450px; width:100%;"/>

Her er et oversikt over de forskjellige skrifttyper som er tilgjengelig og hvordan de ser ut:

<table>
<tr><td style="font-weight: bold;">Font</td><td style="font-weight: bold;">Eksempel Tekst</td></tr>
<tr><td><code>Font::arial</code></td><td><spam style="font-family: arial; font-size: 20pt;">Beskrivende tekst</span></td></tr>
<tr><td><code>Font::arial_bold</code></td><td><spam style="font-family: arial; font-weight: bold; font-size: 20pt;">Beskrivende tekst</span></td></tr>
<tr><td><code>Font::arial_italic</code></td><td><spam style="font-family: arial; font-style: italic; font-size: 20pt;">Beskrivende tekst</span></td></tr>
<tr><td><code>Font::arial_bold_italic</code></td><td><spam style="font-family: arial; font-weight: bold; font-style: italic; font-size: 20pt;">Beskrivende tekst</span></td></tr>
<tr><td><code>Font::courier</code></td><td><spam style="font-family: 'Courier New'; font-size: 20pt;">Beskrivende tekst</span></td></tr>
<tr><td><code>Font::courier_bold</code></td><td><spam style="font-family: 'Courier New'; font-weight: bold; font-size: 20pt;">Beskrivende tekst</span></td></tr>
<tr><td><code>Font::courier_italic</code></td><td><spam style="font-family: 'Courier New'; font-style: italic; font-size: 20pt;">Beskrivende tekst</span></td></tr>
<tr><td><code>Font::courier_bold_italic</code></td><td><spam style="font-family: 'Courier New'; font-weight: bold; font-style: italic; font-size: 20pt;">Beskrivende tekst</span></td></tr>
<tr><td><code>Font::times</code></td><td><spam style="font-family: times; font-size: 20pt;">Beskrivende tekst</span></td></tr>
<tr><td><code>Font::times_bold</code></td><td><spam style="font-family: times; font-weight: bold; font-size: 20pt;">Beskrivende tekst</span></td></tr>
<tr><td><code>Font::times_italic</code></td><td><spam style="font-family: times; font-style: italic; font-size: 20pt;">Beskrivende tekst</span></td></tr>
<tr><td><code>Font::times_bold_italic</code></td><td><spam style="font-family: times; font-weight: bold; font-style: italic; font-size: 20pt;">Beskrivende tekst</span></td></tr>

</table>

### Bilder

For å kunne tegne et bilde må den lastes inn fra en fil. Finn gjerne et bilde på din datamaskin, eller om du har behov for et vilkårlig bilde kan du alternativt bruke denne:

<a title="Dietmar Rabich / Wikimedia Commons / “Dichroitisches Prisma -- 2020 -- 5123” / CC BY-SA 4.0" href="https://commons.wikimedia.org/wiki/File:Dichroitisches_Prisma_--_2020_--_5123.jpg"><img width="256" alt="Dichroitisches Prisma -- 2020 -- 5123" src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Dichroitisches_Prisma_--_2020_--_5123.jpg/512px-Dichroitisches_Prisma_--_2020_--_5123.jpg"></a>

Kopier bildet inn på prosjektet:

<img src="../../images/image_file_view.png" alt="Bilde som viser et JPG fil i arbeidsområdet i Visual Studio Code" style="max-width:300px; width:100%;"/>

Slik laster du inn ditt bilde, og tegner den i vinduet:

```c++
	TDT4102::Point topLeftCorner {50, 100};
	TDT4102::Image image("img.jpg");
	window.draw_image(topLeftCorner, image);
```

Om bilde har et annet navn enn «img.jpg» må du endre navnet som er definert i koden slik at den er akkurat det samme som navnet på filen:

<img src="../../images/image_file_equivalent.png" alt="Bilde som viser et JPG fil i arbeidsområdet i Visual Studio Code med røde bokser" style="max-width:600px; width:100%;"/>

**Merk:** når du skal tegne et bilde i en animasjon bør du ikke laste inn bildet innenfor while løkken. Om du hadde flyttet `image` variablen inn på `while`-løkken, så hadde bildet blitt lastet inn på nytt hver eneste gang et bilde blir tegnet.

```c++
#include "AnimationWindow.h"

int main() {
	TDT4102::AnimationWindow window;

    // VIKTIG: Image variabler bør ikke defineres innenfor while-løkken
    TDT4102::Image image("img.jpg");
    
	while(!window.should_close()) {
		TDT4102::Point topLeftCorner {50, 100};
		window.draw_image(topLeftCorner, image);
		window.next_frame();
	}

	return 0;
}
```

Noen bilder er for store eller for små i sin orginale størrelse. Det er derfor mulig å spesifisere en annen størrelse når du skal tegne bildet. For eksempel, her brukes muspekeren til dette:

```c++
#include "AnimationWindow.h"

int main() {
    TDT4102::AnimationWindow window;

    TDT4102::Image image("img.jpg");

    while(!window.should_close()) {
        TDT4102::Point topLeftCorner {0, 0};
		TDT4102::Point mouse = window.get_mouse_coordinates();
		int imageWidth = mouse.x;
		int imageHeight = mouse.y;
        window.draw_image(topLeftCorner, image, imageWidth, imageHeight);
        window.next_frame();
    }

    return 0; 
}
```

<img src="../../images/image_file_resize.png" alt="Bilde som viser et bilde som blir tegnet med at alternativ størrelse" style="max-width:400px; width:100%;"/>
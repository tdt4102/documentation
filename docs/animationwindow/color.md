## Egendefinerte farger

Klikk her å velge farge: 

<script>
	window.addEventListener("load", startup, false);

	const hex2rgb = (hex) => {
	    const r = parseInt(hex.slice(1, 3), 16);
	    const g = parseInt(hex.slice(3, 5), 16);
	    const b = parseInt(hex.slice(5, 7), 16);
	    
	    return { r, g, b };
	}

	function colourSelectionChanged(event) {
		const rgbValues = hex2rgb(event.target.value);

		// Update text values
		document.getElementById('red0').innerHTML = rgbValues['r'];
		document.getElementById('red1').innerHTML = rgbValues['r'];
		document.getElementById('red2').innerHTML = rgbValues['r'];
		document.getElementById('green0').innerHTML = rgbValues['g'];
		document.getElementById('green1').innerHTML = rgbValues['g'];
		document.getElementById('green2').innerHTML = rgbValues['g'];
		document.getElementById('blue0').innerHTML = rgbValues['b'];
		document.getElementById('blue1').innerHTML = rgbValues['b'];
		document.getElementById('blue2').innerHTML = rgbValues['b'];

		// Make answer region visible
		document.getElementById('resultOutput').style.display = "block";
	}

	function startup() {
		const colorPicker = document.getElementById("colorPicker");
		colorPicker.addEventListener("change", colourSelectionChanged, false);
		colorPicker.setValue("#399AD5");
	}
</script>

<input type="color" id="colorPicker" value="#ff0000" style="width:10em;">

<div id="resultOutput" style="display: none;">

Du kan enten lagre fargen i en egen variabel:
	<div class="highlight"><pre id="__code_1"><span></span><button class="md-clipboard md-icon" title="Copy to clipboard" data-clipboard-target="#__code_1 > code"></button><code><a id="__codelineno-0-1" name="__codelineno-0-1" href="#__codelineno-0-1"></a><span class="n">TDT4102</span><span class="o">::</span><span class="n">Color</span><span class="w"> </span><span class="nf">chosenColor</span><span class="p">(</span><span class="mi" id="red0">100</span><span class="p">,</span><span class="w"> </span><span class="mi" id="green0">100</span><span class="p">,</span><span class="w"> </span><span class="mi" id="blue0">100</span><span class="p">);</span><span class="w"></span></code></pre></div>

Eller bruke verdien direkte, for eksempel som verdi av en funksjonsparameter:
	<div class="highlight"><pre id="__code_1"><span></span><button class="md-clipboard md-icon" title="Copy to clipboard" data-clipboard-target="#__code_1 > code"></button><code><a id="__codelineno-0-1" name="__codelineno-0-1" href="#__codelineno-0-1"></a><span class="n">TDT4102</span><span class="o">::</span><span class="n">Color</span><span class="p">(</span><span class="mi" id="red1">100</span><span class="p">,</span><span class="w"> </span><span class="mi" id="green1">100</span><span class="p">,</span><span class="w"> </span><span class="mi" id="blue1">100</span><span class="p">)</span><span class="w"></span></code></pre></div>

Farger kan representeres på forskjellige måter. Det mest vanlige formatet er RGB (Rødt, Grønt, Blått). De ulike kanalene blandes i forskjellige mengder for å lage farger. Disse pleier å være verdier mellom 0 og 255 og det er de som brukes i Color-klassen. Prøv å velge hvit og deretter svart, og se hva som skjer med RGB-verdiene!

<div class="highlight"><pre id="__code_3"><span></span><button class="md-clipboard md-icon" title="Copy to clipboard" data-clipboard-target="#__code_3 > code"></button><code><a id="__codelineno-0-1" name="__codelineno-0-1" href="#__codelineno-0-1"></a><span class="kt">unsigned</span><span class="w"> </span><span class="kt">char</span><span class="w"> </span><span class="n">redChannel</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="mi" id="red2">0</span><span class="p">;</span><span class="w"></span>
<a id="__codelineno-0-2" name="__codelineno-0-2" href="#__codelineno-0-2"></a><span class="kt">unsigned</span><span class="w"> </span><span class="kt">char</span><span class="w"> </span><span class="n">greenChannel</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="mi" id="green2">0</span><span class="p">;</span><span class="w"></span>
<a id="__codelineno-0-3" name="__codelineno-0-3" href="#__codelineno-0-3"></a><span class="kt">unsigned</span><span class="w"> </span><span class="kt">char</span><span class="w"> </span><span class="n">blueChannel</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="mi" id="blue2">0</span><span class="p">;</span><span class="w"></span>
<a id="__codelineno-0-4" name="__codelineno-0-4" href="#__codelineno-0-4"></a><span class="n">TDT4102</span><span class="o">::</span><span class="n">Color</span><span class="w"> </span><span class="nf">chosenColor</span><span class="p">(</span><span class="n">redChannel</span><span class="p">,</span><span class="w"> </span><span class="n">greenChannel</span><span class="p">,</span><span class="w"> </span><span class="n">blueChannel</span><span class="p">);</span><span class="w"></span>
</code></pre></div>
</div>

## Forhåndsdefinerte farger

Det er vanligvis lettere å bruke en forhåndsdefinert farge i stedet for å måtte lete etter RGB verdier. Derfor har vi forhåndsdefinert en rekke farger. Fargeverdiene du kan velge fra er vist i tabellen under.

<table>
<tr><td style="font-weight: bold;">Color verdi</td><td style="font-weight: bold;">Farge</td></tr>
<tr><td>TDT4102::Color::transparent</td><td style="background-color: rgba(0, 0, 0, 1);">(transparant)</td></tr>
<tr><td>TDT4102::Color::black</td><td style="background-color:#000000;"></td></tr>  
<tr><td>TDT4102::Color::silver</td><td style="background-color:#c0c0c0;"></td></tr>     
<tr><td>TDT4102::Color::gray</td><td style="background-color:#808080;"></td></tr>   
<tr><td>TDT4102::Color::white</td><td style="background-color:#ffffff;"></td></tr>  
<tr><td>TDT4102::Color::maroon</td><td style="background-color:#800000;"></td></tr>     
<tr><td>TDT4102::Color::red</td><td style="background-color:#ff0000;"></td></tr>    
<tr><td>TDT4102::Color::purple</td><td style="background-color:#800080;"></td></tr>        
<tr><td>TDT4102::Color::magenta</td><td style="background-color:#ff00ff;"></td></tr>    
<tr><td>TDT4102::Color::green</td><td style="background-color:#008000;"></td></tr>  
<tr><td>TDT4102::Color::lime</td><td style="background-color:#00ff00;"></td></tr>   
<tr><td>TDT4102::Color::olive</td><td style="background-color:#808000;"></td></tr>  
<tr><td>TDT4102::Color::yellow</td><td style="background-color:#ffff00;"></td></tr>     
<tr><td>TDT4102::Color::navy</td><td style="background-color:#000080;"></td></tr>   
<tr><td>TDT4102::Color::blue</td><td style="background-color:#0000ff;"></td></tr>   
<tr><td>TDT4102::Color::teal</td><td style="background-color:#008080;"></td></tr>   
<tr><td>TDT4102::Color::aqua</td><td style="background-color:#00ffff;"></td></tr>   
<tr><td>TDT4102::Color::orange</td><td style="background-color:#ffa500;"></td></tr>     
<tr><td>TDT4102::Color::alice_blue</td><td style="background-color:#f0f8ff;"></td></tr>     
<tr><td>TDT4102::Color::antique_white</td><td style="background-color:#faebd7;"></td></tr>  
<tr><td>TDT4102::Color::aquamarine</td><td style="background-color:#7fffd4;"></td></tr>     
<tr><td>TDT4102::Color::azure</td><td style="background-color:#f0ffff;"></td></tr>  
<tr><td>TDT4102::Color::beige</td><td style="background-color:#f5f5dc;"></td></tr>  
<tr><td>TDT4102::Color::bisque</td><td style="background-color:#ffe4c4;"></td></tr>     
<tr><td>TDT4102::Color::blanched_almond</td><td style="background-color:#ffebcd;"></td></tr>    
<tr><td>TDT4102::Color::blue_violet</td><td style="background-color:#8a2be2;"></td></tr>    
<tr><td>TDT4102::Color::brown</td><td style="background-color:#a52a2a;"></td></tr>  
<tr><td>TDT4102::Color::burly_wood</td><td style="background-color:#deb887;"></td></tr>     
<tr><td>TDT4102::Color::cadet_blue</td><td style="background-color:#5f9ea0;"></td></tr>     
<tr><td>TDT4102::Color::chart_reuse</td><td style="background-color:#7fff00;"></td></tr>    
<tr><td>TDT4102::Color::chocolate</td><td style="background-color:#d2691e;"></td></tr>  
<tr><td>TDT4102::Color::coral</td><td style="background-color:#ff7f50;"></td></tr>  
<tr><td>TDT4102::Color::cornflower_blue</td><td style="background-color:#6495ed;"></td></tr>    
<tr><td>TDT4102::Color::corn_silk</td><td style="background-color:#fff8dc;"></td></tr>  
<tr><td>TDT4102::Color::crimson</td><td style="background-color:#dc143c;"></td></tr>    
<tr><td>TDT4102::Color::cyan</td><td style="background-color:#00ffff;"></td></tr>   
<tr><td>TDT4102::Color::dark_blue</td><td style="background-color:#00008b;"></td></tr>  
<tr><td>TDT4102::Color::dark_cyan</td><td style="background-color:#008b8b;"></td></tr>  
<tr><td>TDT4102::Color::dark_goldenrod</td><td style="background-color:#b8860b;"></td></tr>     
<tr><td>TDT4102::Color::dark_gray</td><td style="background-color:#a9a9a9;"></td></tr>  
<tr><td>TDT4102::Color::dark_green</td><td style="background-color:#006400;"></td></tr>     
<tr><td>TDT4102::Color::dark_grey</td><td style="background-color:#a9a9a9;"></td></tr>  
<tr><td>TDT4102::Color::dark_khaki</td><td style="background-color:#bdb76b;"></td></tr>     
<tr><td>TDT4102::Color::dark_magenta</td><td style="background-color:#8b008b;"></td></tr>   
<tr><td>TDT4102::Color::dark_olivegreen</td><td style="background-color:#556b2f;"></td></tr>    
<tr><td>TDT4102::Color::dark_orange</td><td style="background-color:#ff8c00;"></td></tr>    
<tr><td>TDT4102::Color::dark_orchid</td><td style="background-color:#9932cc;"></td></tr>    
<tr><td>TDT4102::Color::dark_red</td><td style="background-color:#8b0000;"></td></tr>   
<tr><td>TDT4102::Color::dark_salmon</td><td style="background-color:#e9967a;"></td></tr>    
<tr><td>TDT4102::Color::dark_seagreen</td><td style="background-color:#8fbc8f;"></td></tr>  
<tr><td>TDT4102::Color::dark_slateblue</td><td style="background-color:#483d8b;"></td></tr>     
<tr><td>TDT4102::Color::dark_slategray</td><td style="background-color:#2f4f4f;"></td></tr>     
<tr><td>TDT4102::Color::dark_slategrey</td><td style="background-color:#2f4f4f;"></td></tr>     
<tr><td>TDT4102::Color::dark_turquoise</td><td style="background-color:#00ced1;"></td></tr>     
<tr><td>TDT4102::Color::dark_violet</td><td style="background-color:#9400d3;"></td></tr>    
<tr><td>TDT4102::Color::deep_pink</td><td style="background-color:#ff1493;"></td></tr>  
<tr><td>TDT4102::Color::deep_skyblue</td><td style="background-color:#00bfff;"></td></tr>   
<tr><td>TDT4102::Color::dim_gray</td><td style="background-color:#696969;"></td></tr>   
<tr><td>TDT4102::Color::dim_grey</td><td style="background-color:#696969;"></td></tr>   
<tr><td>TDT4102::Color::dodger_blue</td><td style="background-color:#1e90ff;"></td></tr>    
<tr><td>TDT4102::Color::firebrick</td><td style="background-color:#b22222;"></td></tr>  
<tr><td>TDT4102::Color::floral_white</td><td style="background-color:#fffaf0;"></td></tr>   
<tr><td>TDT4102::Color::forest_green</td><td style="background-color:#228b22;"></td></tr>   
<tr><td>TDT4102::Color::gainsboro</td><td style="background-color:#dcdcdc;"></td></tr>  
<tr><td>TDT4102::Color::ghost_white</td><td style="background-color:#f8f8ff;"></td></tr>    
<tr><td>TDT4102::Color::gold</td><td style="background-color:#ffd700;"></td></tr>   
<tr><td>TDT4102::Color::goldenrod</td><td style="background-color:#daa520;"></td></tr>  
<tr><td>TDT4102::Color::green_yellow</td><td style="background-color:#adff2f;"></td></tr>   
<tr><td>TDT4102::Color::grey</td><td style="background-color:#808080;"></td></tr>   
<tr><td>TDT4102::Color::honeydew</td><td style="background-color:#f0fff0;"></td></tr>   
<tr><td>TDT4102::Color::hot_pink</td><td style="background-color:#ff69b4;"></td></tr>   
<tr><td>TDT4102::Color::indian_red</td><td style="background-color:#cd5c5c;"></td></tr>     
<tr><td>TDT4102::Color::indigo</td><td style="background-color:#4b0082;"></td></tr>     
<tr><td>TDT4102::Color::ivory</td><td style="background-color:#fffff0;"></td></tr>  
<tr><td>TDT4102::Color::khaki</td><td style="background-color:#f0e68c;"></td></tr>  
<tr><td>TDT4102::Color::lavender</td><td style="background-color:#e6e6fa;"></td></tr>   
<tr><td>TDT4102::Color::lavender_blush</td><td style="background-color:#fff0f5;"></td></tr>     
<tr><td>TDT4102::Color::lawn_green</td><td style="background-color:#7cfc00;"></td></tr>     
<tr><td>TDT4102::Color::lemon_chiffon</td><td style="background-color:#fffacd;"></td></tr>  
<tr><td>TDT4102::Color::light_blue</td><td style="background-color:#add8e6;"></td></tr>     
<tr><td>TDT4102::Color::light_coral</td><td style="background-color:#f08080;"></td></tr>    
<tr><td>TDT4102::Color::light_cyan</td><td style="background-color:#e0ffff;"></td></tr>     
<tr><td>TDT4102::Color::light_goldenrodyellow</td><td style="background-color:#fafad2;"></td></tr>  
<tr><td>TDT4102::Color::light_gray</td><td style="background-color:#d3d3d3;"></td></tr>     
<tr><td>TDT4102::Color::light_green</td><td style="background-color:#90ee90;"></td></tr>    
<tr><td>TDT4102::Color::light_grey</td><td style="background-color:#d3d3d3;"></td></tr>     
<tr><td>TDT4102::Color::light_pink</td><td style="background-color:#ffb6c1;"></td></tr>     
<tr><td>TDT4102::Color::light_salmon</td><td style="background-color:#ffa07a;"></td></tr>   
<tr><td>TDT4102::Color::light_sea_green</td><td style="background-color:#20b2aa;"></td></tr>
<tr><td>TDT4102::Color::light_sky_blue</td><td style="background-color:#87cefa;"></td></tr>
<tr><td>TDT4102::Color::light_slate_gray</td><td style="background-color:#778899;"></td></tr>
<tr><td>TDT4102::Color::light_slate_grey</td><td style="background-color:#778899;"></td></tr>
<tr><td>TDT4102::Color::light_steel_blue</td><td style="background-color:#b0c4de;"></td></tr>
<tr><td>TDT4102::Color::light_yellow</td><td style="background-color:#ffffe0;"></td></tr>   
<tr><td>TDT4102::Color::lime_green</td><td style="background-color:#32cd32;"></td></tr>     
<tr><td>TDT4102::Color::linen</td><td style="background-color:#faf0e6;"></td></tr>
<tr><td>TDT4102::Color::medium_aquamarine</td><td style="background-color:#66cdaa;"></td></tr>  
<tr><td>TDT4102::Color::medium_blue</td><td style="background-color:#0000cd;"></td></tr>    
<tr><td>TDT4102::Color::medium_orchid</td><td style="background-color:#ba55d3;"></td></tr>  
<tr><td>TDT4102::Color::medium_purple</td><td style="background-color:#9370db;"></td></tr>  
<tr><td>TDT4102::Color::medium_sea_green</td><td style="background-color:#3cb371;"></td></tr>
<tr><td>TDT4102::Color::medium_slate_blue</td><td style="background-color:#7b68ee;"></td></tr>
<tr><td>TDT4102::Color::medium_spring_green</td><td style="background-color:#00fa9a;"></td></tr>
<tr><td>TDT4102::Color::medium_turquoise</td><td style="background-color:#48d1cc;"></td></tr>   
<tr><td>TDT4102::Color::medium_violet_red</td><td style="background-color:#c71585;"></td></tr>
<tr><td>TDT4102::Color::midnight_blue</td><td style="background-color:#191970;"></td></tr>  
<tr><td>TDT4102::Color::mint_cream</td><td style="background-color:#f5fffa;"></td></tr>     
<tr><td>TDT4102::Color::misty_rose</td><td style="background-color:#ffe4e1;"></td></tr>     
<tr><td>TDT4102::Color::moccasin</td><td style="background-color:#ffe4b5;"></td></tr>   
<tr><td>TDT4102::Color::navajo_white</td><td style="background-color:#ffdead;"></td></tr>   
<tr><td>TDT4102::Color::old_lace</td><td style="background-color:#fdf5e6;"></td></tr>   
<tr><td>TDT4102::Color::olivedrab</td><td style="background-color:#6b8e23;"></td></tr>  
<tr><td>TDT4102::Color::orange_red</td><td style="background-color:#ff4500;"></td></tr>     
<tr><td>TDT4102::Color::orchid</td><td style="background-color:#da70d6;"></td></tr>     
<tr><td>TDT4102::Color::pale_goldenrod</td><td style="background-color:#eee8aa;"></td></tr>     
<tr><td>TDT4102::Color::pale_green</td><td style="background-color:#98fb98;"></td></tr>     
<tr><td>TDT4102::Color::pale_turquoise</td><td style="background-color:#afeeee;"></td></tr>     
<tr><td>TDT4102::Color::pale_violet_red</td><td style="background-color:#db7093;"></td></tr>
<tr><td>TDT4102::Color::papayawhip</td><td style="background-color:#ffefd5;"></td></tr>     
<tr><td>TDT4102::Color::peachpuff</td><td style="background-color:#ffdab9;"></td></tr>  
<tr><td>TDT4102::Color::peru</td><td style="background-color:#cd853f;"></td></tr>   
<tr><td>TDT4102::Color::pink</td><td style="background-color:#ffc0cb;"></td></tr>   
<tr><td>TDT4102::Color::plum</td><td style="background-color:#dda0dd;"></td></tr>   
<tr><td>TDT4102::Color::powder_blue</td><td style="background-color:#b0e0e6;"></td></tr>    
<tr><td>TDT4102::Color::rosy_brown</td><td style="background-color:#bc8f8f;"></td></tr>     
<tr><td>TDT4102::Color::royal_blue</td><td style="background-color:#4169e1;"></td></tr>     
<tr><td>TDT4102::Color::saddle_brown</td><td style="background-color:#8b4513;"></td></tr>   
<tr><td>TDT4102::Color::salmon</td><td style="background-color:#fa8072;"></td></tr>     
<tr><td>TDT4102::Color::sandy_brown</td><td style="background-color:#f4a460;"></td></tr>    
<tr><td>TDT4102::Color::sea_green</td><td style="background-color:#2e8b57;"></td></tr>  
<tr><td>TDT4102::Color::sea_shell</td><td style="background-color:#fff5ee;"></td></tr>  
<tr><td>TDT4102::Color::sienna</td><td style="background-color:#a0522d;"></td></tr>     
<tr><td>TDT4102::Color::sky_blue</td><td style="background-color:#87ceeb;"></td></tr>   
<tr><td>TDT4102::Color::slate_blue</td><td style="background-color:#6a5acd;"></td></tr>     
<tr><td>TDT4102::Color::slate_gray</td><td style="background-color:#708090;"></td></tr>     
<tr><td>TDT4102::Color::slate_grey</td><td style="background-color:#708090;"></td></tr>     
<tr><td>TDT4102::Color::snow</td><td style="background-color:#fffafa;"></td></tr>   
<tr><td>TDT4102::Color::spring_green</td><td style="background-color:#00ff7f;"></td></tr>   
<tr><td>TDT4102::Color::steel_blue</td><td style="background-color:#4682b4;"></td></tr>     
<tr><td>TDT4102::Color::tan</td><td style="background-color:#d2b48c;"></td></tr>    
<tr><td>TDT4102::Color::thistle</td><td style="background-color:#d8bfd8;"></td></tr>    
<tr><td>TDT4102::Color::fuchsia</td><td style="background-color:#ff00ff;"></td></tr> 
<tr><td>TDT4102::Color::tomato</td><td style="background-color:#ff6347;"></td></tr>     
<tr><td>TDT4102::Color::turquoise</td><td style="background-color:#40e0d0;"></td></tr>  
<tr><td>TDT4102::Color::violet</td><td style="background-color:#ee82ee;"></td></tr>     
<tr><td>TDT4102::Color::wheat</td><td style="background-color:#f5deb3;"></td></tr>  
<tr><td>TDT4102::Color::white_smoke</td><td style="background-color:#f5f5f5;"></td></tr>    
<tr><td>TDT4102::Color::yellow_green</td><td style="background-color:#9acd32;"></td></tr>   
<tr><td>TDT4102::Color::rebecca_purple</td><td style="background-color:#663399;"></td></tr>
</table>

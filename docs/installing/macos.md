# Installering

## VS Code

Visual Studio Code installeres fra [nettsiden](https://code.visualstudio.com/download). Etter den er pakket ut kan du dra den over til `Applications`.

Dersom du er glad i å bruke kommandolinjen kan du legge til code til path på følgende vis:
I VS Code: ++cmd+shift+p++ -> "Shell Command: Install 'code' command in PATH".  
Deretter kan du åpne en gitt mappe eller fil fra terminalen med `code <fil/mappe>`.

### TDT4102 Extension

Åpne extensions-skjermen ved å klikke på extensions-knappen på venstre side (1). Søk opp extensionen `TDT4102 Tools` (2).

![Bilde av hvor man kan finne extensions](../images/mac_finn_extension.png)

## Hurtiginstallasjon

1. Trykk på ++cmd+shift+p++ for å åpne kommandopalletten.
2. Skriv inn til du får opp kommandoen: `TDT4102: Install required tools`
3. ++enter++ for å kjøre kommandoen
4. En terminal vil åpne seg og be deg om passord. Her vises verken passordet eller prikker mens du skriver. Deretter vil den be deg om å trykke ++enter++ for å bekrefte installasjonen. ![Bilde av hvor man skriver passord](../images/mac_skriv_passord.png)
5. Vent til installasjonen nede i høyre hjørne er ferdig. Dette kan ta litt tid.
6. ++cmd+shift+p++ deretter `TDT4102: Perform a health check of the setup` for å sjekke at alt ble riktig installert.

## Manuell installasjon

### Xcode Command Line Tools

Kjør `xcode-select --install` i terminalen for å installere Xcode Command Line Tools

### Homebrew

Installer Homebrew ved å kjøre `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"` i terminalen.

HVIS DU HAR M1: Deretter legger du det til på path med kommandoen `echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> ~/.zprofile`. Dette gjør at kommandoen `$(/opt/homebrew/bin/brew shellenv)` kjøres hver gang du åpner terminalen. Kjør den en gang manuelt eller restart terminalen for å få tilgang til kommandoen `brew`.

Verifiser at Homebrew er på PATH ved å kjøre `brew --version`.

### Meson

Installer meson gjennom `brew` med `brew install meson` og deretter kjør `meson --version` for å verifisere at det er på PATH.

### Diverse pakker 

Vi preinstallerer pakkene vi bruker for å ikke måtte kompilere dem for hvert prosjekt

`brew install sdl2 sdl2_image jpeg-turbo libpng libtiff cmake`
# Installering 

**Merk at de to primære platformene som anbefales og støttes i faget TDT4102 er Windows og Mac.** De aller fleste studentene i faget benytter Windows eller Mac, og _fagstaben har ikke kapasitet til å gi veiledning i bruk av Linux_. Denne siden er en ekstra hjelp til dere som bruker Linux. 

## VS Code

Visual Studio Code installeres fra [nettsiden](https://code.visualstudio.com/docs/setup/linux).

Hvis man har snap installert kan man istedenfor laste ned Visual Studio Code med: `sudo snap install --classic code`

### TDT4102 Extension

Åpne extensions-skjermen ved å klikke på extensions-knappen på venstre side (1). Søk opp extensionen `TDT4102 Tools` (2).

![Bilde av hvor man kan finne extensions](../images/linux_finn_extension.png)

## Hurtiginstallasjon

1. Trykk på ++ctrl+shift+p++ for å åpne kommandopalletten.
2. Skriv inn til du får opp kommandoen: `TDT4102: Install required tools`
3. ++enter++ for å kjøre kommandoen
4. En terminal vil åpne seg og be deg om passord. Her vises verken passordet eller prikker mens du skriver. Deretter vil den be deg om å trykke ++enter++ for å bekrefte installasjonen.
5. Vent til installasjonen nede i høyre hjørne er ferdig. Dette kan ta litt tid.
6. ++ctrl+shift+p++ deretter `TDT4102: Perform a health check of the setup` for å sjekke at alt ble riktig installert.


## Manuell installasjon

### Clang 

Debian, Ubuntu og derivater bruker et script som er hentet fra <https://apt.llvm.org/>: `bash -c "$(wget -O - https://apt.llvm.org/llvm.sh)"`  
Fedora, CentOS, RHEL og lignende: `sudo dnf install clang llvm`  
Arch: `sudo pacman -S llvm clang`


### Meson og Ninja

Debian, Ubuntu og derivater: `sudo apt install meson ninja-build`  
Fedora, Centos, RHEL og derivater: `sudo dnf install meson ninja-build`  
Arch: `sudo pacman -S meson ninja`

### Diverse pakker

Installer de korresponderene pakkene via din pakkemanager:
`SDL2`, `SDL2-image`, `cmake`, `libjpeg-turbo`, `libpng`, `libtiff`

Det er ikke påkrevd, men gjør kompilering raskere. 
Merk: Noen pakkemanagere har for gammel versjon av SDL2 og SDL2-image, da må man laste ned og bygge fra kilde. SDL2 har en [guide](https://wiki.libsdl.org/SDL2/Installation) man kan følge. 

## TEST STATUS

Manjaro KDE: fungerer

Debian, Ubuntu o.l. : fungerer

Fedora: fungerer

Arch: fungerer

openSUSE: fungerer


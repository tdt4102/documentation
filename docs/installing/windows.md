# Installering

## VS Code

Visual Studio Code installeres ved hjelp av en installer som du kan laste ned på [nettsiden](https://code.visualstudio.com/download).

Husk å huke av "Add to PATH" og "Register Code as an editor for supported file types". De øvrige valgene er valgbare, men anbefales. 
![Bilde av oppsett av VS Code](../images/win_vscode_setup.png)


### TDT4102 Extension

Åpne extensions-skjermen ved å klikke på extensions-knappen på venstre side (1). Søk opp extensionen `TDT4102 Tools` (2).

![Bilde av hvor man kan finne extensions](../images/win_finn_extension.png)

## Hurtiginstallasjon
1. Trykk på ++cmd+shift+p++ for å åpne kommandopalletten.
2. Skriv inn til du får opp kommandoen: `TDT4102: Install required tools`
3. ++enter++ for å kjøre kommandoen
4. ++cmd+shift+p++ deretter `TDT4102: Perform a health check of the setup` for å verifisere at installasjonen var suksessfull.

## Manuell installasjon

### Meson

Meson kan også installeres fra en msi fra deres [github releases](https://github.com/mesonbuild/meson/releases). Last ned releasen som har en msi-installer, dvs. filnavnet slutter med `.msi`. Dobbeltklikk på fila for å starte installeren. 

###  Clang gjennom MinGW

MinGW lastes ned som en zip som vi har hentet fra [winlibs.com](https://winlibs.com/). Vi hoster den på NTNU´s servere for å ikke overbelaste winlibs og slik at det skal være lettere å velge riktig versjon. Last den ned [her](https://ntnu.box.com/shared/static/b7yqk2gvkacuw624bc0flzenu86cp7r7.zip). Deretter pakk ut zippen til `C:\`. Ikke pakk den ut en annen plass, da blir extensionen forvirret. Endre navn på mappen til `TDT4102-mingw64` hvis den allerede ikke heter det.

I tillegg må `C:\TDT4102-mingw64\bin` [legges til på PATH](https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/). Merk at dersom du har windows på norsk heter "environment variables" "systemmiljøvariabler". Følgende variabler må settes: 

|Navn | Verdi |
|-----|-------|
| CC  | C:\TDT4102-mingw64\bin\clang|
| CXX | C:\TDT4102-mingw64\bin\clang++|
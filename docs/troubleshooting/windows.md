# Feilsøking

Det kan skje noe galt under installering eller under kjøring av et prosjekt. Under er det listet et par ting det kan være lurt å sjekke hvis det dukker opp feilmeldinger på Windows. Et generelt tips er å kjøre `TDT4102: Force refresh of the course content` og `TDT4102: Perform health check of the setup` fra kommandopaletten for en kjapp sjekk av oppsettet. Hvis dette ikke løser problemet, og noe fortsatt er galt, gå gjennom punktene under.  

### Meson og MinGW

Meson skal ligge under `C:\Program Files` og TDT4102-mingw64 skal ligge under `C:\`. Hvis noen av mappene mangler, kjør `TDT4102: Install required tools`. 

### Systemmiljøvariabler og PATH

Både Meson og MinGW må ligge på PATH og systemmiljøvariablene må være riktig. Gå til Windows søk og skriv inn "systemmiljøvariabler", velg alternativet "Rediger systemmiljøvariabler" øverst. Trykk på knappen "Miljøvariabler...", og sjekk at variabelene `CC` og `CXX` er hhv. satt til `C:\TDT4102-mingw64\bin\clang` og `C:\TDT4102-mingw64\bin\clang++`. Hvis de ikke gjør det, legg de til manuelt med knappen "Ny...". Deretter dobbeltklikk på variabelen "Path". Sjekk at `C:\Program Files\Meson\` og `C:\TDT4102-mingw64\bin` står på lista, hvis de ikke gjør det, legg også de til manuelt med knappen "Ny". 

### Filsti og mappe

Pass på at mappen/prosjektet du jobber i ikke er syncet med en skytjeneste som OneDrive, DropBox, Google Drive etc. Sjekk også at filstien ikke inneholder æøå og spesielle symboler som mellomrom, $, % o.l. Bindestrek og understrek går fint. 


### Piazza

Hvis ingen av punktene hjalp, sjekk om noen har spurt om noe lignende på Piazza eller lag et eget innlegg. 
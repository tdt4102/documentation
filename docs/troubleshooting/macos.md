# Feilsøking

Det kan skje noe galt under installering eller under kjøring av et prosjekt. Under er det listet et par ting det kan være lurt å sjekke hvis det dukker opp feilmeldinger på MacOS. Et generelt tips er å kjøre `TDT4102: Force refresh of the course content` og `TDT4102: Perform health check of the setup` fra kommandopaletten for en kjapp sjekk av oppsettet. Hvis dette ikke løser problemet, og noe fortsatt er galt, gå gjennom punktene under.


### Filsti og mappe

Pass på at mappen/prosjektet du jobber i ikke er syncet med en skytjeneste som ICloud, OneDrive, DropBox, Google Drive etc. Sjekk også at filstien ikke inneholder æøå og spesielle symboler som mellomrom, $, % o.l. Bindestrek og understrek går fint. 

### Homebrew og XCode

Åpne Terminal og kjør `brew doctor`. Dersom noe er feil, start fra toppen og kjør kommandoene som Homebrew foreslår. Hvis Homebrew klager på versjonen av Mac'en, oppdater den. Hvis Homebrew klager på versjonen av `XCode` eller `Command Line Tools`, kjør `xcode-select -p` og lim inn filstien etter `sudo rm -rf `. Dette avinstallerer XCode og dette må installeres på nytt. Dette kan gjøres med `TDT4102: Install required tools` fra kommandopaletten i VS Code. 

### Kjøring av program

Hvis ingen feilmeldinger dukker under kjøring og ingenting dukker opp (utskrift i terminal eller Animationwindow) så må det kompileres manuelt med `meson compile -Cbuilddir` i prosjektmappen og deretter kan det kjøres med F5. Dersom F5 heller ikke funker, gå inn i builddir-mappa `cd builddir` og kjør `./program` i terminalen.

### Piazza

Hvis ingen av punktene hjalp, sjekk om noen har spurt om noe lignende på Piazza eller lag et eget innlegg. 


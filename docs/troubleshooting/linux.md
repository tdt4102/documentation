# Feilsøking

Det kan skje noe galt under installering eller under kjøring av et prosjekt. Under er det listet et par ting det kan være lurt å sjekke hvis det dukker opp feilmeldinger på Linux. Et generelt tips er å kjøre `TDT4102: Force refresh of the course content` og `TDT4102: Perform health check of the setup` fra kommandopaletten for en kjapp sjekk av oppsettet. 

### PATH

Verifiser at meson, ninja og clang er på PATH.

### Piazza

Hvis ingen av punktene hjalp, sjekk om noen har spurt om noe lignende på Piazza eller lag et eget innlegg.
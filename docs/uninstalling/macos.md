# Avinstallering

1. Data fra extensions finnes på `~/Library/Application Support/Code/User/globalStorage/tdt4102ntnu.tdt4102-tools`. Denne mappen MÅ slettes
2. `~/Library/tdt4102/`. Denne mappen kan trygt slettes
3. `~/.vscode/extensions/`. Her ligger det en mappe som har tdt4102 i navnet. Denne kan slettes.


### Visual Studio Code

VS Code er nesten aldri kilden til problemet da det bare er en teksteditor. Allikevel viser vi hvordan det avinstalleres. Merk at `Library` og `.vscode` er skjulte mapper:

1. Applikasjonen finnes i applikasjonsmappen. Flytt den til papirkurven.
2. Data fra extensions finnes på `~/Library/Application Support/Code/User/globalStorage/tdt4102ntnu.tdt4102-tools`. Slett denne mappen

### Xcode Command Line Tools

Finn mappen der de er installert: `xcode-select -p`.  
Fjern mappen der de er installert: `sudo rm -rf <Filsti>`.

### Avinstallering av pakker og Homebrew

Resten er installert via `brew`. De avinstalleres med `brew uninstall meson sdl2 sdl2_image jpeg-turbo libpng libtiff cmake`  

Brew kan avinstalleres ved hjelp av avinstalleringsskriptet: `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/uninstall.sh)"`
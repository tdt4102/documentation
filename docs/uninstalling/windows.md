# Avinstallering

Data fra extensions ligger under mappen `AppData`, denne er skjult by default. 

1. `%appdata%/Code/User/globalStorage/tdt4102ntnu.tdt4102-tools`. Denne mappen MÅ slettes.
2. `%appdata%/tdt4102`. Hele denne mappen kan trygt slettes.
3. Mer data finnes på `%userprofile%/.vscode/extensions/`. Der skal det ligge en mappe som heter noe med tdt4102 i. Den kan dere slette.

### Visual Studio Code

VS Code er nesten aldri kilden til problemet da det bare er en teksteditor. Allikevel viser vi hvordan det avinstalleres:

1. Avinstaller "vanlig" fra Innstillinger->Apper->Apper og funksjoner.
2. Data fra extensions finnes på `%appdata%/Code/User/globalStorage/tdt4102ntnu.tdt4102-tools`. Slett denne mappen

### Meson

Meson har en fungerende uninstaller, avinstaller som vanlig fra Instillinger->Apper->Apper og funksjoner.

### MinGW

Alt MinGW-relatert installeres i `C:/TDT4102-mingw64`. Bare slett den mappen.
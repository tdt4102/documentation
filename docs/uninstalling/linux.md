# Avinstallering

1. Data fra extensions finnes på `~/.config/Code/User/globalStorage/tdt4102ntnu.tdt4102-tools`. Denne mappen MÅ slettes
2. `~/.config/Code/CachedExtensionVSIXs/tdt4102ntnu.tdt4102-tools/`. Denne mappen kan trygt slettes
3. `~/.vscode/extensions/`. Her ligger det en mappe som har tdt4102 i navnet. Denne kan også slettes.

### Visual Studio Code
VS Code er nesten aldri kilden til problemet da det bare er en teksteditor. Allikevel viser vi hvordan det avinstalleres:

Hvis Visual Studio Code er installert via snap så avinstaller med `sudo snap remove code`

Ellers avinstaller på vanlig måte gjennom din pakkemanager, f. eks. `apt`: 

`sudo apt remove code`

### Avinstallering av pakker

Debian, Ubuntu og derivater: `sudo apt remove meson ninja-build`

Fedora, Centos, RHEL og derivater:  `sudo dnf remove meson ninja-build clang llvm`

Arch: `sudo pacman -R meson llvm clang ninja`

Avinstaller de korresponderene pakkene via din pakkemanager:
`SDL2`, `SDL2-image`, `cmake`, `libjpeg-turbo`, `libpng`, `libtiff`
